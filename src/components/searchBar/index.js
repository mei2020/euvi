import PropTypes from 'prop-types';
import { PureComponent } from 'react';
import { Icon } from '../icon';

import './index.scss';

export class SearchBar extends PureComponent {
  render() {
    return (
      <div className='search-bar'>
        <Icon name='magnify' />
        <input
          type='text'
          onChange={(e) => {
            this.props.onChange(e.target.value);
          }}
          placeholder='Search...'
        />
      </div>
    );
  }
}

SearchBar.propTypes = {
  onChange: PropTypes.func,
};

SearchBar.defaultProps = {
  onChange: () => {},
};
