import PropTypes from 'prop-types';
import { PureComponent } from 'react';

import './index.scss';

export class Icon extends PureComponent {
  render() {
    return <i className={`icon icon-${this.props.name}`}></i>;
  }
}

Icon.propTypes = {
  name: PropTypes.string.isRequired,
};
