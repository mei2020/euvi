import PropTypes from 'prop-types';
import { Fragment } from 'react';

import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

import { Icon } from '../icon';
import { NavIcon } from './nav-icon';

export function Link({ isPrimary, icon, label, onClick, path }) {
  return (
    <NavLink
      onClick={onClick}
      className={classNames({
        link: isPrimary,
        'list-link': !isPrimary,
      })}
      to={path}
    >
      {isPrimary ? (
        <NavIcon label={label} icon={icon}></NavIcon>
      ) : (
        <Fragment>
          <Icon name={icon} /> <span>{label}</span>
        </Fragment>
      )}
    </NavLink>
  );
}

Link.defaultProps = {
  isPrimary: false,
  onClick: () => {},
};

Link.propTypes = {
  isPrimary: PropTypes.bool,
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  path: PropTypes.string.isRequired,
};
