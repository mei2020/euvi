import PropTypes from 'prop-types';
import { PureComponent } from 'react';
import { Icon } from '../icon';

export class NavIcon extends PureComponent {
  render() {
    return (
      <div className='nav-icon'>
        <Icon name={this.props.icon} />
        <span className='label'>{this.props.label}</span>
      </div>
    );
  }
}

NavIcon.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string,
};
