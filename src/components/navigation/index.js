import { createRef, PureComponent } from 'react';
import PullElement from 'pull-element';
import classNames from 'classnames';

import './index.scss';
import { Link } from './link';
import { userService } from '../../services/user';

export class Navigation extends PureComponent {
  constructor(props) {
    super(props);
    this.containerRef = createRef();
    this.secondaryRef = createRef();
    this.state = {
      navigation: userService.getNavigation() || { primary: [], secondary: [] },
    };
  }

  updateNav = (e) => {
    const hadSecondary = this.state.navigation.secondary.length > 0;
    this.setState({ navigation: e.detail }, () => {
      if (!hadSecondary && this.state.navigation.secondary.length > 0) {
        this.addPullElement();
      } else if (hadSecondary && this.state.navigation.secondary.length === 0) {
        this.removePullElement();
      }
    });
  };

  closeNavigation = () => {
    this.pullElement?.animateTo(0, 0);
  };

  addPullElement() {
    const secondaryHeight = this.secondaryRef.current?.scrollHeight;
    this.pullElement = new PullElement({
      target: this.containerRef.current,
      wait: false,
      detectBoundary: true,
      onPullDown({ translateY }) {
        if (translateY >= 0) {
          this.preventDefault();
          this.setTranslate(0, 0);
        }
      },
      onPullUp({ translateY }) {
        if (translateY < -secondaryHeight) {
          this.preventDefault();
          this.setTranslate(0, -secondaryHeight);
        }
      },
      onPullUpEnd({ translateY }) {
        if (-translateY >= 30) {
          this.preventDefault();
          this.animateTo(0, -secondaryHeight);
        }
      },
    });
    this.pullElement.init();
  }

  removePullElement() {
    this.pullElement?.destroy();
  }

  componentDidMount() {
    document.addEventListener('navigationUpdated', this.updateNav);
    if (this.state.navigation.secondary.length) {
      this.addPullElement();
    }
  }

  componentWillUnmount() {
    this.removePullElement();
  }

  render() {
    const { navigation } = this.state;
    return (
      <div
        className={classNames('navigation', {
          'has-pullup': navigation.secondary?.length,
        })}
        ref={this.containerRef}
      >
        <div className='primary'>
          {navigation.primary?.map((route) => (
            <Link
              onClick={this.closeNavigation}
              key={`r_${route.path}`}
              isPrimary
              path={route.path}
              label={route.label}
              icon={route.icon}
            />
          ))}
        </div>
        <div className='secondary' ref={this.secondaryRef}>
          {navigation.secondary?.map((route) => (
            <Link
              onClick={this.closeNavigation}
              key={`r_${route.path}`}
              path={route.path}
              label={route.label}
              icon={route.icon}
            />
          ))}
        </div>
      </div>
    );
  }
}
