import PropTypes from 'prop-types';
import { PureComponent } from 'react';

import './index.scss';

export class PendingItem extends PureComponent {
  render() {
    return (
      <li className='item' key={this.props.visitor.name}>
        <p className='name'>{this.props.visitor.name}</p>
        <div className='checkbox-am'>
          <input
            className='checkbox'
            defaultChecked={this.props.visitor.am}
            type='checkbox'
          />
        </div>
        <div className='checkbox-pm'>
          <input
            className='checkbox'
            defaultChecked={this.props.visitor.pm}
            type='checkbox'
          />
        </div>
        <div className='confirm'>
          <button onClick={this.props.onRemove}>Confirm</button>
        </div>
        <div className='comment'>{this.props.visitor.comment}</div>
      </li>
    );
  }
}

PendingItem.propTypes = {
  visitor: PropTypes.shape({
    name: PropTypes.string,
    comment: PropTypes.string,
    pm: PropTypes.bool,
    am: PropTypes.bool,
  }).isRequired,
  onRemove: PropTypes.func.isRequired,
};
