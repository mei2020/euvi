import PropTypes from 'prop-types';
import { createRef, Fragment, PureComponent } from 'react';
import classNames from 'classnames';

import { PendingItem } from './pending-item';
import { CAPACITY } from '../../common/floor-capacity';

import './index.scss';
import { Icon } from '../icon';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const INITIAL_HEIGHT = '100%';

export class PendingList extends PureComponent {
  state = {
    collapsed: false,
    listHeight: INITIAL_HEIGHT,
  };

  constructor(props) {
    super(props);
    this.listRef = createRef();
  }

  componentDidMount() {
    if (this.state.listHeight === INITIAL_HEIGHT && this.props.list.length) {
      setTimeout(() => {
        this.setState({ listHeight: this.listRef.current.scrollHeight });
      }, 300);
    }
  }

  getListStyles() {
    const { collapsed, listHeight } = this.state;
    return collapsed ? { maxHeight: 0 } : { maxHeight: listHeight };
  }

  handleHeaderClick = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  getUniqueFloors() {
    var floors = [],
      flags = [];
    for (var i = 0; i < this.props.list.length; i++) {
      if (flags[this.props.list[i].floor]) continue;
      flags[this.props.list[i].floor] = true;
      floors.push(this.props.list[i].floor);
    }
    return floors;
  }

  getVisitorByFloor(floor) {
    var visitorsPendings = [];
    this.props.list.map((visitor) => {
      if (visitor.floor === floor) {
        visitorsPendings.push(visitor);
      }
    });
    return visitorsPendings;
  }

  getFloorCapacity(floor, date) {
    var floorCapacity = 0;
    this.props.capacities.map((capacity) => {
      if (
        capacity.floor == floor &&
        capacity.date.getTime() == date.getTime()
      ) {
        floorCapacity = capacity;
      }
    });
    return floorCapacity;
  }

  render() {
    const { title, onRemoveVisitor } = this.props;
    return (
      <div className='pending-list'>
        <h2
          className={classNames('list-title', {
            collapsed: this.state.collapsed,
          })}
          onClick={this.handleHeaderClick}
        >
          {title}
          <Icon name='chevron-left' />
        </h2>
        <div
          ref={this.listRef}
          className='date-request-list'
          style={this.getListStyles()}
        >
          {this.getUniqueFloors().map((floor, i) => {
            const floorCapacity = this.getFloorCapacity(
              floor,
              this.props.list[0].date
            );
            return (
              <Fragment key={i}>
                <div className='floor-list'>
                  <div className='floor-item'>
                    <h3 className='floor'>Floor {floor}</h3>
                    <p className='header-am'>
                      am
                      <br />
                      <span
                        className={
                          floorCapacity.am >= floorCapacity.capacity
                            ? 'capacity overCapacity'
                            : 'capacity'
                        }
                      >
                        {floorCapacity.am}/{floorCapacity.capacity}
                      </span>
                    </p>
                    <p className='header-pm'>
                      pm
                      <br />
                      <span
                        className={
                          floorCapacity.pm >= floorCapacity.capacity
                            ? 'capacity overCapacity'
                            : 'capacity'
                        }
                      >
                        {floorCapacity.pm}/{floorCapacity.capacity}
                      </span>
                    </p>
                    <p></p>
                  </div>
                  <hr />
                </div>
                <TransitionGroup className='request-list'>
                  {this.getVisitorByFloor(floor).map((visitor) => (
                    <CSSTransition
                      key={`f_${i}_vid_${visitor.id}`}
                      classNames='slide-out-right'
                      timeout={300}
                    >
                      <PendingItem
                        key={visitor.id}
                        visitor={visitor}
                        onRemove={() => {
                          onRemoveVisitor(visitor);
                        }}
                      />
                    </CSSTransition>
                  ))}
                </TransitionGroup>
              </Fragment>
            );
          })}
        </div>
      </div>
    );
  }
}

PendingList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  title: PropTypes.string,
  capacities: PropTypes.arrayOf(PropTypes.object).isRequired,
  onRemoveVisitor: PropTypes.func,
};

PendingList.defaultProps = {
  capacities: CAPACITY,
  onRemoveVisitor: () => {},
};
