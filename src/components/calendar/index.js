import PropTypes from 'prop-types';
import { PureComponent } from 'react';
import { times } from 'lodash';
import classNames from 'classnames';

import { Icon } from '../icon';

import './index.scss';
import { MONTHS } from '../../common/months';

const WEEKDAYS = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

const weekdaysTemplate = WEEKDAYS.map((day, index) => (
  <div className='weekday' key={`wd-${index}`}>
    {day}
  </div>
));

export class Calendar extends PureComponent {
  today = new Date();

  state = {
    today: this.today,
    month: this.today.getMonth(),
    year: this.today.getFullYear(),
  };

  selectedDays = [];

  componentDidMount() {
    this.getDays(this.state.month, this.state.year);
  }

  componentDidUpdate(prevProps) {
    if (this.props.searchTerm !== prevProps.searchTerm) {
      this.getDays(this.state.month, this.state.year);
    }
  }

  getDays(monthIndex, year) {
    const endMonth = new Date(year, monthIndex + 1, 0);
    const bleedingDays = new Date(year, monthIndex, 1).getDay();
    const lastMonthDays = new Date(year, monthIndex, 0).getDate();

    const list = [
      ...times(bleedingDays, (index) => (
        <div className='day prev-month' key={`lmd-${index + 1}`}>
          {lastMonthDays - bleedingDays + index + 1}
        </div>
      )),
      ...times(endMonth.getDate(), (index) => {
        const date = new Date(year, monthIndex, index + 1);
        const full = this.props.service.isDayFull(date);
        return (
          <div
            className={classNames('day', {
              clickable: this.props.service.isClickable(date) === true,
              full,
              available: !full,
              closed: this.props.service.isClosed(date),
              selected: this.selectedDays.includes(date.getTime()),
              today: date.toDateString() === this.today.toDateString(),
              booked: this.props.service.isBooked(date, this.props.searchTerm),
            })}
            onClick={() => this.selectDay(date)}
            key={`md-${index + 1}`}
          >
            {index + 1}
          </div>
        );
      }),
    ];
    if (list.length % 7) {
      list.push(
        times(7 - (list.length % 7), (index) => (
          <div className='day next-month' key={`nmd-${index + 1}`}>
            {index + 1}
          </div>
        ))
      );
    }
    this.setState({ daysTemplate: list });
  }

  selectDay = (date) => {
    if (
      this.props.service.isClosed(date) ||
      this.props.service.isClickable(date) === false
    ) {
      return false;
    }

    const { selectedDays } = this;
    const index = selectedDays.indexOf(date.getTime());
    if (index === -1) {
      if (!this.props.multiSelection) {
        selectedDays.pop();
      }
      selectedDays.push(date.getTime());
    } else {
      selectedDays.splice(index, 1);
    }
    this.getDays(this.state.month, this.state.year);

    try {
      this.props.onSelect(selectedDays);
    } catch (e) {
      console.error('Tried to select before initialization');
    }
  };

  moveMonth(increment) {
    const step = this.state.month + increment;
    const month = step >= 0 ? step % MONTHS.length : MONTHS.length - 1;
    const year =
      step >= 0
        ? step < 12
          ? this.state.year
          : this.state.year + 1
        : this.state.year - 1;
    this.selectedDays = [];
    this.getDays(month, year);
    this.setState({ month, year });
  }

  renderMonth() {
    return `${MONTHS[this.state.month]} ${this.state.year}`;
  }

  render() {
    return (
      <div className='calendar-container'>
        <div className='calendar'>
          <h2 className='month'>{this.renderMonth()}</h2>
          <div className='controls'>
            <button onClick={() => this.moveMonth(-1)}>
              <Icon name='chevron-left' />
            </button>
            <button onClick={() => this.moveMonth(1)}>
              <Icon name='chevron-right' />
            </button>
          </div>
          {weekdaysTemplate}
          {this.state.daysTemplate}
        </div>
      </div>
    );
  }
}

Calendar.propTypes = {
  service: PropTypes.object.isRequired,
  multiSelection: PropTypes.bool,
  searchTerm: PropTypes.string,
  onSelect: () => {},
  onChange: PropTypes.func,
};

Calendar.defaultProps = {
  multiSelection: false,
  onSelect: PropTypes.func,
  onChange: PropTypes.func,
};
