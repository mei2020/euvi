import PropTypes from 'prop-types';
import { createRef, Fragment, PureComponent } from 'react';
import classNames from 'classnames';

import { VisitorItem } from './visitor-item';
import { VisitorLogItem } from './visitor-log-item';

import './index.scss';
import { Icon } from '../icon';

const INITIAL_HEIGHT = '100%';

export class VisitorList extends PureComponent {
  state = {
    collapsed: false,
    listHeight: INITIAL_HEIGHT,
  };

  constructor(props) {
    super(props);
    this.listRef = createRef();
  }

  componentDidMount() {
    if (this.state.listHeight === INITIAL_HEIGHT && this.props.list.length) {
      this.setState({ listHeight: this.listRef.current.scrollHeight });
    }
  }

  getListStyles() {
    const { collapsed, listHeight } = this.state;
    return collapsed ? { maxHeight: 0 } : { maxHeight: listHeight };
  }

  handleHeaderClick = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  render() {
    const { list, searchTerm, title, visitors } = this.props;
    return (
      <Fragment>
        {this.props.isAccessControl ? (
          <h2
            className={classNames('list-title', {
              collapsed: this.state.collapsed,
            })}
            onClick={this.handleHeaderClick}
          >
            {title}
            <Icon name='chevron-left' />
          </h2>
        ) : (
          <div className='log-header'>
            <h2
              className={classNames('list-title', 'header-log-title', {
                collapsed: this.state.collapsed,
              })}
              onClick={this.handleHeaderClick}
            >
              {title}
            </h2>
            <span className='header-am'>am</span>
            <span className='header-pm'>pm</span>
          </div>
        )}
        <ul
          ref={this.listRef}
          className='visitor-list'
          style={this.getListStyles()}
        >
          {this.props.isAccessControl
            ? list
                .filter((visitor) => {
                  return (
                    searchTerm === '' ||
                    visitor.toLowerCase().includes(searchTerm)
                  );
                })
                .map((visitor) => (
                  <VisitorItem
                    key={visitor}
                    name={visitor}
                    onChange={this.props.onChange}
                  />
                ))
            : visitors.map((visitor) => (
                <VisitorLogItem
                  key={visitor.id}
                  visitor={visitor}
                  onChange={this.props.onChange}
                />
              ))}
        </ul>
      </Fragment>
    );
  }
}

VisitorList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.string),
  searchTerm: PropTypes.string,
  title: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  isAccessControl: PropTypes.bool,
  visitors: PropTypes.arrayOf(PropTypes.object),
};

VisitorList.defaultProps = {
  onChange: () => {},
  isAccessControl: false,
  list: [],
  visitors: [],
};
