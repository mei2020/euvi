import PropTypes from 'prop-types';
import { PureComponent } from 'react';
import { Icon } from '../icon';

import './index.scss';

export class VisitorLogItem extends PureComponent {
  render() {
    return (
      <li className='log-item' key={this.props.visitor.id}>
        <p className='log-name'>{this.props.visitor.name}</p>
        <div className='log-am'>
          {this.props.visitor.am && <Icon name='check-simple' />}
        </div>
        <div className='log-pm'>
          {this.props.visitor.pm && <Icon name='check-simple' />}
        </div>
      </li>
    );
  }
}

VisitorLogItem.propTypes = {
  visitor: PropTypes.object.isRequired,
  onChange: PropTypes.func,
};
