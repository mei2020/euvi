import PropTypes from 'prop-types';
import { PureComponent } from 'react';

import './index.scss';

export class VisitorItem extends PureComponent {
  state = {
    checked: false,
  };

  onClick = () => {
    const newChecked = !this.state.checked;
    this.setState({ checked: newChecked });
    this.props.onChange(this.props.name, newChecked);
  };

  render() {
    const { checked } = this.state;
    return (
      <li className='item' key={this.props.name} onClick={this.onClick}>
        <p className='name'>{this.props.name}</p>
        <input
          className='checkbox'
          type='checkbox'
          checked={checked}
          onChange={() => {}}
        />
      </li>
    );
  }
}

VisitorItem.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

VisitorItem.defaultProps = {
  onChange: () => {},
};
