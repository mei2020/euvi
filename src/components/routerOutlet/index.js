import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { Route, Switch, useLocation } from 'react-router-dom';

import { routes } from '../../routes';

import './index.scss';

function getTransitionClassname(location) {
  switch (location.pathname) {
    case '/booking':
      return 'slideCalendarDown';
    default:
      return 'slideUp';
  }
}

export function RouterOutlet() {
  const location = useLocation();
  return (
    <TransitionGroup className='router-outlet'>
      <CSSTransition
        key={`cr_${location.pathname}`}
        classNames={getTransitionClassname(location)}
        timeout={300}
      >
        <Switch location={location}>
          {routes.map(({ path, component }) => (
            <Route key={`r_${path}`} path={path}>
              {component}
            </Route>
          ))}
        </Switch>
      </CSSTransition>
    </TransitionGroup>
  );
}
