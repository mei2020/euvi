import PropTypes from 'prop-types';
import { Fragment, PureComponent } from 'react';
import moment from 'moment';
import { sortBy } from 'lodash';

import { Icon } from '../icon';
import { hashCode } from '../../services/utils';

import './index.scss';

export class BookingsList extends PureComponent {
  componentDidMount() {
    this.titleHash = hashCode(this.props.title);
  }

  render() {
    const bookings = sortBy(this.props.bookings, ['date']);
    return (
      <div className='bookings-list'>
        <h2 className='title'>{this.props.title}</h2>
        <span className='header-am'>Morning</span>
        <span className='header-pm'>Afternoon</span>
        {bookings.map((booking, index) => {
          return (
            <Fragment key={`b_${this.titleHash}_${index}`}>
              <span className='date'>
                {moment(booking.date).format('MMMM Do YYYY')}
              </span>
              <div className='checkbox-am'>
                {booking.am && <Icon name='check-simple' />}
              </div>
              <div className='checkbox-pm'>
                {booking.pm && <Icon name='check-simple' />}
              </div>
            </Fragment>
          );
        })}
      </div>
    );
  }
}

BookingsList.propTypes = {
  title: PropTypes.string.isRequired,
  bookings: PropTypes.arrayOf(PropTypes.object),
};
