export const CAPACITY = [
  {
    floor: 1,
    date: new Date(2021, 0, 29),
    capacity: 15,
    am: 15,
    pm: 12,
  },
  {
    floor: 4,
    date: new Date(2021, 0, 29),
    capacity: 20,
    am: 15,
    pm: 20,
  },
  {
    floor: 1,
    date: new Date(2021, 1, 2),
    capacity: 15,
    am: 13,
    pm: 15,
  },
];
