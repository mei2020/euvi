export const MY_BOOKINGS = [
  {
    date: new Date(2020, 10, 30),
    floor: 1,
    am: true,
    pm: false,
    comment: 'I would like to go to office to stay with my coworkers.',
  },
  {
    date: new Date(2020, 11, 31),
    floor: 1,
    am: true,
    pm: true,
    comment: 'I want to introduce my dog to my boss.',
  },
  {
    date: new Date(2020, 11, 30),
    floor: 1,
    am: true,
    pm: true,
    comment: 'I want to introduce my dog to my boss.',
  },
  {
    date: new Date(2021, 0, 28),
    floor: 1,
    am: false,
    pm: true,
    comment: 'I will have an important face-to-face meeting in the afternoon.',
  },
];
