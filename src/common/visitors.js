export const VISITORS = [
  'João Mendes',
  'Mariana Santos',
  'Pedro Silva',
  'Nuno Mendes',
  'Francisco Santos',
  'Ana Silva',
  'Clara Matos',
  'Sandra Ventura',
  'José Constâncio',
  'Mariana Martins',
  'Rui Lopes',
  'Maria Mil-Homens',
  'Mariana Pedro',
  'Jacinto Jota',
  'Fátima Alves',
  'Roberto Palito',
  'Rute Silva',
  'Manuel Cardoso',
  'António Carreira',
  'Francisca Fernandes',
  'Catarina Andrade',
  'José Viegas',
  'André Miguel',
  'João Silva',
  'Rute Marlene',
];
