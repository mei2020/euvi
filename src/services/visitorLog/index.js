import { PENDINGS } from '../../common/pendings';

const DEFAULT_MAX_CAPACITY = 30;

class VisitorLogService {
  constructor() {
    this.today = new Date();
    this.availabilityMap = {};
    this.visitors = PENDINGS;
  }

  isDayFull(date) {
    let dayAvailability = this.availabilityMap[date.getTime()];
    if (!dayAvailability) {
      this.availabilityMap[date.getTime()] = {
        capacity: DEFAULT_MAX_CAPACITY,
        occupancy: 0,
        isBooked: false,
        isClosed: date.getDay() === 0 || date.getDay() === 6,
        isClickable: date.getDay() !== 0 && date.getDay() !== 6,
      };
      return false;
    }

    return dayAvailability.occupancy >= dayAvailability.capacity;
  }

  isClosed(date) {
    return this.availabilityMap[date.getTime()].isClosed;
  }

  isClickable(date) {
    return this.availabilityMap[date.getTime()].isClickable;
  }

  isBooked(date, searchTerm = '') {
    this.availabilityMap[date.getTime()].isBooked = false;
    if (searchTerm.length > 2) {
      this.visitors
        .filter((visitor) => {
          return searchTerm === '' || visitor.name.toLowerCase() === searchTerm;
        })
        .map((visitor) => {
          if (visitor.date.getTime() === date.getTime()) {
            this.availabilityMap[date.getTime()].isBooked = true;
          }
        });
    }
    return this.availabilityMap[date.getTime()].isBooked;
  }

  bookDay(date) {
    this.availabilityMap[date.getTime()].isBooked = true;
  }
}

export const visitorLogService = new VisitorLogService();
