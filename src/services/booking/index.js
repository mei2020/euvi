import { MY_BOOKINGS } from '../../common/my-bookings';

const DEFAULT_MAX_CAPACITY = 30;

class BookingService {
  constructor() {
    this.today = new Date();
    this.availabilityMap = {};
    this.myBookings = MY_BOOKINGS;
  }

  isDayFull(date) {
    let dayAvailability = this.availabilityMap[date.getTime()];
    if (!dayAvailability) {
      this.availabilityMap[date.getTime()] = {
        capacity: DEFAULT_MAX_CAPACITY,
        occupancy: 0,
        isBooked: [], // array of 'am' and or 'pm'
        isClosed:
          date.getDay() === 0 ||
          date.getDay() === 6 ||
          date.getTime() < this.today.getTime(),
      };
      return false;
    }

    return dayAvailability.occupancy >= dayAvailability.capacity;
  }

  isClosed(date) {
    return this.availabilityMap[date.getTime()].isClosed;
  }

  isClickable(date) {
    return this.availabilityMap[date.getTime()].isClickable;
  }

  isBooked(date) {
    return (
      this.availabilityMap[date.getTime()].isBooked.length > 0 ||
      this.myBookings.findIndex((booking) => {
        return date.getTime() === booking.date.getTime();
      }) !== -1
    );
  }

  bookDays(dates, periods, comment) {
    dates.forEach((date) => {
      this.availabilityMap[date].isBooked = periods;
      this.myBookings.push({
        date: new Date(date),
        floor: 1,
        am: periods.includes('am'),
        pm: periods.includes('pm'),
        comment,
      });
    });
  }

  getMyBookings() {
    return this.myBookings;
  }
}

export const bookingService = new BookingService();
