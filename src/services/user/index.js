export const USER_ROLE = {
  VISITOR: 'Visitor',
  ADMINISTRATOR: 'Administrator',
  SECURITY: 'Security',
};

export const USER_ROLES = [
  USER_ROLE.VISITOR,
  USER_ROLE.ADMINISTRATOR,
  USER_ROLE.SECURITY,
];

const NAVIGATION = {
  [USER_ROLE.ADMINISTRATOR]: {
    primary: [
      { label: 'Pending', icon: 'account-plus', path: '/pendings' },
      { label: 'My Bookings', icon: 'calendar', path: '/my-bookings' },
      { label: 'Settings', icon: 'settings', path: '/settings' },
    ],
    secondary: [
      { label: 'Visitor Log', icon: 'history', path: '/visitor-log' },
      { label: 'Metrics', icon: 'pulse', path: '/dashboard' },
    ],
  },
  [USER_ROLE.VISITOR]: {
    primary: [
      { label: 'My Bookings', icon: 'calendar', path: '/my-bookings' },
      { label: 'Settings', icon: 'settings', path: '/settings' },
    ],
    secondary: [],
  },
  [USER_ROLE.SECURITY]: {
    primary: [
      { label: "Today's Visits", icon: 'building-1', path: '/access-control' },
      { label: 'My Bookings', icon: 'calendar', path: '/my-bookings' },
      { label: 'Settings', icon: 'settings', path: '/settings' },
    ],
    secondary: [],
  },
};

class UserService {
  constructor() {
    this.role = localStorage.getItem('userRole');
    this.updateNavigation();
  }

  updateNavigation() {
    const event = new CustomEvent('navigationUpdated', {
      detail: NAVIGATION[this.role],
    });
    document.dispatchEvent(event);
  }

  getNavigation() {
    return NAVIGATION[this.role];
  }

  setRole(value) {
    this.role = value;
    localStorage.setItem('userRole', value);
    this.updateNavigation();
  }

  getLandingPage(role) {
    const landingPages = {
      [USER_ROLE.VISITOR]: '/my-bookings',
      [USER_ROLE.ADMINISTRATOR]: '/pendings',
      [USER_ROLE.SECURITY]: '/access-control',
    };
    return landingPages[role];
  }

  removeRole() {
    this.role = null;
    localStorage.removeItem('userRole');
  }
}

export const userService = new UserService();
