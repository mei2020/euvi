import { AccessControl } from './pages/accessControl';
import { Booking } from './pages/booking';
import { MyBookings } from './pages/myBookings';
import { Settings } from './pages/settings';
import { AccountInfo } from './pages/settings/accountInfo';
import { Contact } from './pages/settings/contact';
import { FAQ } from './pages/settings/faq';
import { Terms } from './pages/settings/terms';
import { Pendings } from './pages/pendings';
import { VisitorLog } from './pages/visitorLog';
import { Login } from './pages/login';
import { Dashboard } from './pages/dashboard';

export const routes = [
  {
    path: '/visitor-log',
    component: <VisitorLog />,
  },
  {
    path: '/dashboard',
    component: <Dashboard />,
  },
  {
    path: '/login',
    component: <Login />,
  },
  {
    path: '/access-control',
    component: <AccessControl />,
  },
  {
    path: '/booking',
    component: <Booking />,
  },
  {
    path: '/settings/account-information',
    component: <AccountInfo />,
  },
  {
    path: '/settings/contact',
    component: <Contact />,
  },
  {
    path: '/settings/faq',
    component: <FAQ />,
  },
  {
    path: '/settings/terms',
    component: <Terms />,
  },
  {
    path: '/settings',
    component: <Settings />,
  },
  {
    path: '/my-bookings',
    component: <MyBookings />,
  },
  {
    path: '/pendings',
    component: <Pendings />,
  },
  {
    path: '/',
    component: <Login />,
  },
];
