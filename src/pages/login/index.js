import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { userService, USER_ROLES } from '../../services/user';

import './index.scss';

export class Login extends PureComponent {
  handleRoleSelect = (role) => {
    userService.setRole(role);
  };

  render() {
    return (
      <div className='page login'>
        <header>
          <h2 className='title'>This page is not for production</h2>
          <p>
            Please select the role you wish to test. You may logout to switch
            roles
          </p>
        </header>
        <main>
          {USER_ROLES.map((role) => (
            <Link key={role} to={userService.getLandingPage(role)}>
              <button
                className='button button-border'
                onClick={() => {
                  this.handleRoleSelect(role);
                }}
              >
                {role}
              </button>
            </Link>
          ))}
        </main>
        <footer className='footer'>
          <p>In production, your role would be assigned through your login</p>
        </footer>
        <div className='hide-navigation'></div>
      </div>
    );
  }
}
