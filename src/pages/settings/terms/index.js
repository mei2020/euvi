import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Icon } from '../../../components/icon';
import data from './terms.json';
import './index.scss';

export class Terms extends PureComponent {
  render() {
    return (
      <div className='page page-route terms'>
        <Link to='/settings'>
          <Icon name='chevron-left' />
        </Link>
        <h2>Terms and conditions for Save a Spot</h2>
        {data.map((data, index) => {
          return (
            <div className='terms-item' key={index}>
              <h3>{data.topic}</h3>
              <p>{data.text}</p>
              <p className='bold-info'>{data.boldText}</p>
            </div>
          );
        })}
      </div>
    );
  }
}
