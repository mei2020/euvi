import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Icon } from '../../../components/icon';
import data from './faq.json';
import './index.scss';

export class FAQ extends PureComponent {
  render() {
    return (
      <div className='page page-route faq'>
        <Link to='/settings'>
          <Icon name='chevron-left' />
        </Link>
        {data.map((item, index) => {
          return (
            <div className='faq-item' key={index}>
              <h2>{item.question}</h2>
              <p>{item.answer}</p>
            </div>
          );
        })}
      </div>
    );
  }
}
