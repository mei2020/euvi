import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Icon } from '../../components/icon';

import { userService } from '../../services/user';

import './index.scss';

export class Settings extends PureComponent {
  render() {
    return (
      <div className='page page-route settings'>
        <h1>Settings</h1>
        <div className='settings-group'>
          <Link to='/settings/account-information'>
            <p>
              <span className='setting-description'>Account information</span>
              <span className='setting-value'>
                <Icon name='chevron-right' />
              </span>
              <hr />
            </p>
          </Link>
          <p>
            <span className='setting-description'>Notifications</span>
            <span className='setting-value'>
              <input type='checkbox' />
            </span>
            <hr />
          </p>
          <p>
            <span className='setting-description'>Language</span>
            <span className='setting-value'>English</span>
            <hr />
          </p>
        </div>
        <div className='settings-group'>
          <Link to='/settings/faq'>
            <p>
              <span className='setting-description'>FAQ</span>
              <span className='setting-value'>
                <Icon name='chevron-right' />
              </span>
              <hr />
            </p>
          </Link>
          <Link to='/settings/contact'>
            <p>
              <span className='setting-description'>Contact</span>
              <span className='setting-value'>
                <Icon name='chevron-right' />
              </span>
              <hr />
            </p>
          </Link>
          <Link to='/settings/terms'>
            <p>
              <span className='setting-description'>Terms and Conditions</span>
              <span className='setting-value'>
                <Icon name='chevron-right' />
              </span>
              <hr />
            </p>
          </Link>
        </div>
        <div className='settings-group'>
          <Link to='/login' onClick={() => userService.removeRole}>
            <p>
              <span className='setting-description'>Logout</span>
              <span className='setting-value'>
                <Icon name='chevron-right' />
              </span>
              <hr />
            </p>
          </Link>
        </div>
      </div>
    );
  }
}
