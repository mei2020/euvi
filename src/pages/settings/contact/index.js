import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Icon } from '../../../components/icon';
import './index.scss';

export class Contact extends PureComponent {
  render() {
    return (
      <div className='page page-route contact'>
        <Link to='/settings'>
          <Icon name='chevron-left' />
        </Link>
        <h2>Client Support</h2>
        <p className='bold-info'>711 000 000</p>
        <p>Monday to Saturday, 9h to 21h</p>
        <br />
        <p className='bold-info'>Talk with us</p>
        <p>Do you need some information or want to give a suggestion?</p>
        <p className='bold-info'>Fill the form to be contacted.</p>
        <input type='text' placeholder='Subject' />
        <textarea placeholder='Message' rows='6' />
      </div>
    );
  }
}
