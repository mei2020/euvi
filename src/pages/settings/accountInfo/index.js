import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Icon } from '../../../components/icon';
import './index.scss';

export class AccountInfo extends PureComponent {
  render() {
    return (
      <div className='page page-route account-info'>
        <Link to='/settings'>
          <Icon name='chevron-left' />
        </Link>
        <div className='account-header'>
          <div className='account-image'>
            <Icon name='account-circle' />
          </div>
          <p className='name'>João Filipe Neto Mendes</p>
        </div>
        <div className='account-body'>
          <p>
            <Icon name='account-card-details' />
            10000000
          </p>
          <hr />
          <p>
            <Icon name='calendar-today' />
            31-02-1990
          </p>
          <hr />
          <p>
            <Icon name='map-marker' />
            Rua do Piolho N°10, Lisboa
          </p>
          <hr />
          <p>
            <Icon name='gender-transgender' />
            Masculino
          </p>
          <hr />
          <p>
            <Icon name='phone' />
            911 000 000
          </p>
          <hr />
          <p>
            <Icon name='at' />
            joni_mendes@email.com
          </p>
          <hr />
        </div>
      </div>
    );
  }
}
