import { Component } from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';

import { Calendar } from '../../components/calendar';
import { PENDINGS } from '../../common/pendings';
import { visitorLogService } from '../../services/visitorLog';
import { VisitorList } from '../../components/visitors/visitor-list';

import './index.scss';

export class VisitorLog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      daysSelected: [],
      suggestions: [],
    };
  }

  getSuggestions = (value) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0
      ? []
      : this.props.visitors.filter((lang) =>
          lang.name.toLowerCase().includes(inputValue)
        );
  };

  getSuggestionValue = (suggestion) => suggestion.name;

  renderSuggestion = (suggestion) => (
    <div className='autocomplete-suggestion'>{suggestion.name}</div>
  );

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue.toLowerCase(),
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  handleSelect = (days) => {
    this.setState({ daysSelected: days });
  };

  visitorsByDay(date) {
    var visitorList = [];
    this.props.visitors.forEach((visitor) => {
      if (visitor.date.getTime() === date) {
        visitorList.push(visitor);
      }
    });
    if (!visitorList.length) {
      visitorList.push({ id: 1, name: 'No visitors for the selected day' });
    }
    return visitorList;
  }

  render() {
    var list = this.visitorsByDay(this.state.daysSelected[0]);
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Search by name...',
      value,
      onChange: this.onChange,
    };
    return (
      <div className='page page-route visitor-log'>
        <div className='page-topnav'>
          <h1>Visitor Log</h1>
          <Autosuggest
            onChange={this.handleSearch}
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={this.getSuggestionValue}
            renderSuggestion={this.renderSuggestion}
            inputProps={inputProps}
          />
        </div>
        <Calendar
          service={visitorLogService}
          onSelect={this.handleSelect}
          searchTerm={this.state.value}
          onChange={this.handleSelectIn}
        />
        {list ? (
          <div className='page-content'>
            <VisitorList
              title='Visitors'
              visitors={list}
              searchTerm={this.state.value}
              onChange={this.handleSelectIn}
            ></VisitorList>
          </div>
        ) : null}
      </div>
    );
  }
}

VisitorLog.propTypes = {
  visitors: PropTypes.arrayOf(PropTypes.object).isRequired,
};

VisitorLog.defaultProps = {
  visitors: PENDINGS,
};
