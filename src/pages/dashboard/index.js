import { PureComponent } from 'react';
import dash from './Dashboard.svg';

import './index.scss';

export class Dashboard extends PureComponent {
  render() {
    return (
      <div className='page dashboard'>
        <img className='dashboard-img' src={dash} />
      </div>
    );
  }
}
