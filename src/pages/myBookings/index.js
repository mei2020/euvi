import { PureComponent } from 'react';
import { Link } from 'react-router-dom';

import { BookingsList } from '../../components/bookings/bookings-list';
import { Calendar } from '../../components/calendar';
import { bookingService } from '../../services/booking';

import './index.scss';

export class MyBookings extends PureComponent {
  render() {
    const today = new Date().getTime();
    return (
      <div className='page-route my-bookings'>
        <Link to='/booking'>
          <header className='page header'>
            <h1>My Bookings</h1>
            <Calendar service={bookingService} />
          </header>
        </Link>
        <main className='page main'>
          <BookingsList
            title='Upcoming'
            bookings={bookingService
              .getMyBookings()
              .filter((booking) => booking.date.getTime() > today)}
          />
          <BookingsList
            title='Past Bookings'
            bookings={bookingService
              .getMyBookings()
              .filter((booking) => booking.date.getTime() < today)}
          />
        </main>
      </div>
    );
  }
}
