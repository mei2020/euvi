import { Component } from 'react';
import classNames from 'classnames';

import { Calendar } from '../../components/calendar';
import { bookingService } from '../../services/booking';

import './index.scss';
import { Link } from 'react-router-dom';

export class Booking extends Component {
  state = {
    daysSelected: [],
    periodsSelected: ['am', 'pm'],
    comment: '',
  };

  handleBook = () => {
    const { daysSelected, periodsSelected, comment } = this.state;
    bookingService.bookDays(daysSelected, periodsSelected, comment);
  };

  handleComment = (e) => {
    this.setState({ comment: e.target.value });
  };

  handleSelect = (daysSelected) => {
    this.setState({ daysSelected });
  };

  handlePeriod(period) {
    const periodsSelected = [...this.state.periodsSelected];
    if (periodsSelected.includes(period)) {
      periodsSelected.splice(periodsSelected.indexOf(period), 1);
    } else {
      periodsSelected.push(period);
    }
    this.setState({ periodsSelected });
  }

  render() {
    const { daysSelected, periodsSelected } = this.state;
    return (
      <div className='page page-route booking'>
        <div className='calendar-container'>
          <Calendar
            onSelect={this.handleSelect}
            multiSelection
            service={bookingService}
          />
          {daysSelected.length > 0 && (
            <div className='footer'>
              <div className='period-selector'>
                <div className='label'>Period of the day:</div>
                <div className='checkbox'>
                  <label className='header'>am</label>
                  <input
                    type='checkbox'
                    defaultChecked
                    onChange={() => {
                      this.handlePeriod('am');
                    }}
                  ></input>
                </div>
                <div className='checkbox'>
                  <label className='header'>pm</label>
                  <input
                    type='checkbox'
                    defaultChecked
                    onChange={() => {
                      this.handlePeriod('pm');
                    }}
                  ></input>
                </div>
              </div>
              <label className='label'>Reason to visit the office:</label>
              <input
                type='text'
                className='textarea'
                placeholder='State why you need to visit the office'
                onChange={this.handleComment}
              />
              <div className='actions'>
                <Link to='/my-bookings'>
                  <button
                    className={classNames('button booked', {
                      disabled: periodsSelected.length === 0,
                    })}
                    disabled={periodsSelected.length === 0}
                    onClick={this.handleBook}
                  >
                    Book
                  </button>
                </Link>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
