import PropTypes from 'prop-types';
import { PureComponent } from 'react';
import { PENDINGS } from '../../common/pendings';
import { PendingList } from '../../components/pendings/pending-list';

export class Pendings extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      visitors: props.values,
      options: {
        month: 'long',
        day: 'numeric',
      },
    };
  }

  getUniqueDates() {
    var dates = [],
      flags = [];
    for (var i = 0; i < this.state.visitors.length; i++) {
      if (
        flags[this.state.visitors[i].date] ||
        this.state.visitors[i].date.getTime() < Date.now()
      ) {
        continue;
      }
      flags[this.state.visitors[i].date] = true;
      dates.push(this.state.visitors[i].date);
    }
    return dates;
  }

  getVisitorByDate(date) {
    var visitorsPendings = [];
    this.state.visitors.map((visitor) => {
      if (visitor.date.getTime() == date.getTime()) {
        visitorsPendings.push(visitor);
      }
    });
    return visitorsPendings;
  }

  removeVisitor = (visitor) => {
    const visitors = [...this.state.visitors];
    const visitorIndex = visitors.findIndex((e) => e.id === visitor.id);
    if (visitorIndex !== -1) {
      visitors.splice(visitorIndex, 1);
      this.setState({ visitors });
    }
  };

  render() {
    return (
      <div className='page page-route pendings'>
        <h1>Pending</h1>
        {this.getUniqueDates().map((date, i) => {
          return (
            <PendingList
              key={i}
              title={date.toLocaleDateString('en-US', this.state.options)}
              list={this.getVisitorByDate(date)}
              onRemoveVisitor={this.removeVisitor}
            />
          );
        })}
      </div>
    );
  }
}

Pendings.propTypes = {
  values: PropTypes.arrayOf(PropTypes.dict).isRequired,
};

Pendings.defaultProps = {
  values: PENDINGS,
};
