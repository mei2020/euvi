import PropTypes from 'prop-types';
import { PureComponent } from 'react';
import { VISITORS } from '../../common/visitors';

import { MONTHS } from '../../common/months';

import { SearchBar } from '../../components/searchBar';
import { VisitorList } from '../../components/visitors/visitor-list';

import './index.scss';

export class AccessControl extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      listCheckIn: props.values,
      listCheckOut: [],
      selectedIn: [],
      selectedOut: [],
      searchTerm: '',
    };
  }

  getDate = () => {
    var currentDate = new Date();
    return MONTHS[currentDate.getMonth()] + ' ' + currentDate.getDate() + 'th';
  };

  handleSearch = (term) => {
    this.setState({
      searchTerm: term.toLowerCase(),
    });
  };

  handleConfirm = () => {
    const { listCheckIn, listCheckOut, selectedIn, selectedOut } = this.state;
    const newListCheckIn = [...listCheckIn];
    const newListCheckOut = [...listCheckOut];

    selectedIn.forEach((checkIn) => {
      newListCheckIn.splice(newListCheckIn.indexOf(checkIn), 1);
    });

    selectedOut.forEach((checkOut) => {
      newListCheckOut.splice(newListCheckOut.indexOf(checkOut), 1);
    });
    newListCheckOut.push(...selectedIn);

    this.setState({
      listCheckIn: newListCheckIn,
      listCheckOut: newListCheckOut,
      selectedIn: [],
      selectedOut: [],
    });
  };

  handleSelectIn = (id, checked) => {
    const { selectedIn } = this.state;
    if (checked) {
      selectedIn.push(id);
      this.setState({ selectedIn });
    } else {
      selectedIn.splice(selectedIn.indexOf(id), 1);
      this.setState({ selectedIn });
    }
  };

  handleSelectOut = (id, checked) => {
    const { selectedOut } = this.state;
    if (checked) {
      selectedOut.push(id);
      this.setState({ selectedOut });
    } else {
      selectedOut.splice(selectedOut.indexOf(id), 1);
      this.setState({ selectedOut });
    }
  };

  render() {
    return (
      <div className='page access-control'>
        <div className='page-topnav'>
          <h1>{this.getDate()}</h1>
          <SearchBar onChange={this.handleSearch} />
        </div>
        <div className='page-content'>
          <VisitorList
            title='Out of Office'
            list={this.state.listCheckIn}
            searchTerm={this.state.searchTerm}
            onChange={this.handleSelectIn}
            isAccessControl={true}
          ></VisitorList>
          <VisitorList
            title='Inside Office'
            list={this.state.listCheckOut}
            searchTerm={this.state.searchTerm}
            onChange={this.handleSelectOut}
            isAccessControl={true}
          ></VisitorList>
        </div>
        <div className='page-actions'>
          <button type='button' onClick={this.handleConfirm}>
            Confirm
          </button>
        </div>
      </div>
    );
  }
}

AccessControl.propTypes = {
  values: PropTypes.arrayOf(PropTypes.string).isRequired,
};

AccessControl.defaultProps = {
  values: VISITORS,
};
