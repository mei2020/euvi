import { Component } from 'react';
import { HashRouter } from 'react-router-dom';

import { Navigation } from './components/navigation';
import { RouterOutlet } from './components/routerOutlet';

export default class App extends Component {
  render() {
    return (
      <div className='App'>
        <HashRouter>
          <Navigation />
          <div className='content'>
            <RouterOutlet />
          </div>
        </HashRouter>
      </div>
    );
  }
}
